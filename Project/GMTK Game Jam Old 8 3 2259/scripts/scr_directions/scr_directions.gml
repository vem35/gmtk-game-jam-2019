enum directional
{
	north = -2,
	east = 1,
	south = 2,
	west = -1,
	center = 0
}