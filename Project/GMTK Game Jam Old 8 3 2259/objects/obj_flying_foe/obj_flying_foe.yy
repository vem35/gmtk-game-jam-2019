{
    "id": "d9edbc7c-85ec-4f44-a0ef-167a6c3e837c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_flying_foe",
    "eventList": [
        {
            "id": "437fd96d-c9c5-4155-932e-e97cab90706e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d9edbc7c-85ec-4f44-a0ef-167a6c3e837c"
        },
        {
            "id": "469a7d4e-3f7e-4c84-8b20-2837edd260ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d9edbc7c-85ec-4f44-a0ef-167a6c3e837c"
        },
        {
            "id": "e5c77dfb-fcff-4a3b-95b0-df2fceaedb6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d88c0bcc-bca7-42ae-8e20-75d5e04136f4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d9edbc7c-85ec-4f44-a0ef-167a6c3e837c"
        },
        {
            "id": "d9ff22b5-50e5-4cd7-8d44-ec07980ec3ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d9edbc7c-85ec-4f44-a0ef-167a6c3e837c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d9edbc7c-85ec-4f44-a0ef-167a6c3e837c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "aae68a76-ab33-4d31-8a77-76ad86478b85",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "500",
            "varName": "range",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "2de89a82-211d-4505-ab3b-0e148034fa5f",
    "visible": true
}