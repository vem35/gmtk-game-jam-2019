{
    "id": "abcedbd8-8630-4b00-85b4-1ca87863c39c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objPlatform",
    "eventList": [
        {
            "id": "e62f60cb-8613-4ab2-862f-f118c7f71c5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "abcedbd8-8630-4b00-85b4-1ca87863c39c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "63772afc-cdf8-48e8-8ece-6c4188130c2c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "a107ea08-71a7-4070-9554-f01288aa747a",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "x_start",
            "varType": 0
        },
        {
            "id": "2ee68de9-d3ff-4fbb-8274-6c91c4f88983",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "x_end",
            "varType": 0
        },
        {
            "id": "570fbbf4-aaa4-4efe-ab54-dd7df17444a5",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "y_start",
            "varType": 0
        },
        {
            "id": "0274d652-8113-4eea-a974-f54125efec0d",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "y_end",
            "varType": 0
        },
        {
            "id": "448b837a-d843-4a29-acfb-46600a9438e7",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "3",
            "varName": "hspd",
            "varType": 0
        },
        {
            "id": "c25e6f80-8238-4c4f-a469-362ea8cdc860",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "vspd",
            "varType": 0
        },
        {
            "id": "3b3ca63a-0451-4c53-b485-4764e9168d2c",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "1",
            "varName": "dir",
            "varType": 0
        },
        {
            "id": "3e3e1a8f-2161-4591-8f88-a6a8e8f35ad3",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "True",
            "varName": "enabled",
            "varType": 3
        }
    ],
    "solid": false,
    "spriteId": "3538bd69-4c94-47d6-828f-ea8c4510014a",
    "visible": true
}