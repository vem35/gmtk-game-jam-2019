/// @description Insert description here
// You can write your code in this editor


if (door)
{
	image_angle = -90
}
else
{
switch (facing)
	{
		case directional.north:
			image_angle = 90
			break
		case directional.west:
			image_angle = 180
			break
		case directional.south:
			image_angle = -90
			break
		default:
			image_angle = 0
	
	}
	//Shoot ----------------------------------
	//TODO make it so you can shoot in the opposite direction
	fireDelay = 0;

	fireDelay -= 1
	if(scr_input(gcontrols.shoot, cinput.press) && fireDelay < 0 && instance_number(objBullet) < 3)
	{
		var bx = bbox_right
		var by = bbox_top
	
		switch(facing)
		{
			case directional.north:
				by = bbox_top
				break
			case directional.south:
				by = bbox_bottom
				break
			case directional.east:
				bx = bbox_right
				break
			case directional.west:
				bx = bbox_left
				break
		}
	
	
		fireDelay = 3;
		var f = facing
		with(instance_create_layer(bx, by, "Player", objBullet))
		{
			hamt = 0
			vamt = 0
			switch(f)
			{
				case directional.north:
					vamt = -1
					break
				case directional.south:
					vamt = 1
					break
				case directional.west:
					hamt = -1
					break
				case directional.east:
					hamt = 1
					break
			}
		}
	}
}



if(scr_input(gcontrols.gend, cinput.press))
	game_end()
else if (scr_input(gcontrols.restart, cinput.press))
	room_restart()