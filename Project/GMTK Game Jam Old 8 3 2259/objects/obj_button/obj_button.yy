{
    "id": "2cf1125c-dd84-414d-b0c2-81498157b83e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button",
    "eventList": [
        {
            "id": "842bb4dd-de5a-460c-8251-685d9e6d0d3c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "2cf1125c-dd84-414d-b0c2-81498157b83e"
        },
        {
            "id": "ec6ef5f7-8944-4bab-ab5b-964aeecb37ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2cf1125c-dd84-414d-b0c2-81498157b83e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "d0e4b529-5a0e-4d0c-9e53-2b02c94a034c",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "True",
            "varName": "enabled",
            "varType": 3
        },
        {
            "id": "6c2ab273-82ab-4a74-91a9-8fe978ce9448",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "objPlatform",
            "varName": "obj",
            "varType": 5
        },
        {
            "id": "ad8aa436-4364-4f58-aaad-bca96431935a",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "instance",
            "varType": 0
        },
        {
            "id": "ee510b9e-c390-4712-a548-567785e819de",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "1",
                "2"
            ],
            "multiselect": true,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "state",
            "varType": 6
        }
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}