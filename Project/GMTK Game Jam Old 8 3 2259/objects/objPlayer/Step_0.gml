/// @description TODO Player Movement

//controls are arrow keys to move, up arrow jump, Z pick up door, X shoot, space switch rooms

if(keyboard_check(vk_space)) global.time_scale = 0.1
else global.time_scale = 1

var vx_intend = hsp;
var vy_intend = vsp;

//show_debug_message(variable_global_get("time_scale"))

if(control > 0)
{
	
	var intend = scr_player_move_control(vx_intend, vy_intend)
	vx_intend = intend[0]
	vy_intend = intend[1]
	
}

if(not grounded)
{

	if(hasDoor)
		vy_intend -= grav * door_grav_mod * global.time_scale;
	else
		vy_intend -= grav * global.time_scale
	
	if(abs(vy_intend) > teminal_v)
		vy_intend = -teminal_v * door_grav_mod
		
}

var vel = scr_player_motion(vx_intend, vy_intend)
scr_player_platform()

hsp = vel[0]
vsp = vel[1]


//door stuff -------------------------

//door placement
if(door == noone)
{
	
	if(point_in_circle(doorSpotV.x, doorSpotV.y, x, y, 300) && scr_input(gcontrols.interact, cinput.press))
	{
		targetSpot = instance_nearest(x, y, doorSpotV)
		doorOnWall = false;
	} 
	else if(point_in_circle(doorSpotH.x, doorSpotH.y, x, y, 300) && scr_input(gcontrols.interact, cinput.press))
	{
		targetSpot = instance_nearest(x, y, doorSpotH)
		
		doorOnWall = true;
	} 
	else
	{
		targetSpot = noone;	
	}

	
	if(targetSpot != noone && scr_input(gcontrols.interact, cinput.press))
	{
		sprite_index = sprPlayer;

		instance_create_layer(targetSpot.x, targetSpot.y, "Door", objDoor)

		hasDoor = false;
		door = objDoor;
		door.onWall = doorOnWall;
		doorInteracted = true;
	}
}


//door pickup
if(door != noone && !doorInteracted  && !doorInteracted)
{
	if(point_in_circle(door.x, door.y, x, y, 300) && scr_input(gcontrols.interact, cinput.press))
	{
		instance_destroy(door)
		sprite_index = sprPlayerDoor
		hasDoor = true
		door = noone
	}
}

doorInteracted = false;