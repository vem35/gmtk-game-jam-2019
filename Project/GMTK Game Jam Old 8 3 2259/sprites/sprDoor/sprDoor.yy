{
    "id": "343d5869-7717-45ec-80e3-06fb0a2fa69b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprDoor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90923770-dc14-43ee-bc1f-eee33fca0ad0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "343d5869-7717-45ec-80e3-06fb0a2fa69b",
            "compositeImage": {
                "id": "b8823f50-75af-4c26-9e91-9a29697e060b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90923770-dc14-43ee-bc1f-eee33fca0ad0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f622117f-412f-4c43-9435-b73b2bca2786",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90923770-dc14-43ee-bc1f-eee33fca0ad0",
                    "LayerId": "ba97397a-1978-4b87-851f-9f56cc1c04e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "ba97397a-1978-4b87-851f-9f56cc1c04e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "343d5869-7717-45ec-80e3-06fb0a2fa69b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}