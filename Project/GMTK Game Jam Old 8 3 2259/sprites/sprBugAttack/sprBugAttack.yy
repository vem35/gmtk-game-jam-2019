{
    "id": "cc5633dd-90a6-4bd1-9a5d-6220c36d7f65",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBugAttack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 110,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1416891c-c777-493d-a232-d22be500a085",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc5633dd-90a6-4bd1-9a5d-6220c36d7f65",
            "compositeImage": {
                "id": "f7d52272-b231-4574-b7b1-a8651b97deb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1416891c-c777-493d-a232-d22be500a085",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47928497-dbba-40c6-95e5-725978718c62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1416891c-c777-493d-a232-d22be500a085",
                    "LayerId": "a2fcfc82-6d86-49d9-80b7-d312d5e3795d"
                }
            ]
        },
        {
            "id": "65583a41-7af6-4426-bac0-bd58e2430dcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc5633dd-90a6-4bd1-9a5d-6220c36d7f65",
            "compositeImage": {
                "id": "e97d9da0-8e38-49ec-934c-6ac610fc8604",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65583a41-7af6-4426-bac0-bd58e2430dcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cac0f89-0e77-4231-9820-56a4b69dad84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65583a41-7af6-4426-bac0-bd58e2430dcd",
                    "LayerId": "a2fcfc82-6d86-49d9-80b7-d312d5e3795d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a2fcfc82-6d86-49d9-80b7-d312d5e3795d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc5633dd-90a6-4bd1-9a5d-6220c36d7f65",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 111,
    "xorig": 55,
    "yorig": 64
}