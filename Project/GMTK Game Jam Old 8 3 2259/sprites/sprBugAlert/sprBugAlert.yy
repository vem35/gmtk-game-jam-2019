{
    "id": "e0b02c3a-a357-4d53-be1f-cbb0773f70b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBugAlert",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 97,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5fe30b66-c06a-40cf-8c84-64728e7af3a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0b02c3a-a357-4d53-be1f-cbb0773f70b2",
            "compositeImage": {
                "id": "14fe74fd-a143-4131-afce-ea7b0234b075",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fe30b66-c06a-40cf-8c84-64728e7af3a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44c2e1d7-9708-4173-a274-63d34d6ea646",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fe30b66-c06a-40cf-8c84-64728e7af3a6",
                    "LayerId": "c78f2acd-9d87-4747-9d4b-72c8b5b06913"
                }
            ]
        },
        {
            "id": "910c10d8-bba2-447f-90ef-a8e4d8afffd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0b02c3a-a357-4d53-be1f-cbb0773f70b2",
            "compositeImage": {
                "id": "dac86504-3bf3-4b9f-94b2-965e1a22bc10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "910c10d8-bba2-447f-90ef-a8e4d8afffd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f7ea014-1622-44a0-a729-c32fb1960605",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "910c10d8-bba2-447f-90ef-a8e4d8afffd4",
                    "LayerId": "c78f2acd-9d87-4747-9d4b-72c8b5b06913"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "c78f2acd-9d87-4747-9d4b-72c8b5b06913",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0b02c3a-a357-4d53-be1f-cbb0773f70b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 98,
    "xorig": 49,
    "yorig": 64
}