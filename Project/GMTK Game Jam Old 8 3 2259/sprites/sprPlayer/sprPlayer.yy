{
    "id": "7d342613-407e-453b-b39a-1244691f5a18",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 253,
    "bbox_left": 22,
    "bbox_right": 103,
    "bbox_top": 46,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f0186f79-4deb-4d48-97f5-a02cc102364b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d342613-407e-453b-b39a-1244691f5a18",
            "compositeImage": {
                "id": "1abcac8e-4aca-4be4-9506-478051c064dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0186f79-4deb-4d48-97f5-a02cc102364b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d566bcea-b648-442a-8b54-cbcdae0c57e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0186f79-4deb-4d48-97f5-a02cc102364b",
                    "LayerId": "eee69b09-ac28-4512-9f40-63352bb65ddf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "eee69b09-ac28-4512-9f40-63352bb65ddf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d342613-407e-453b-b39a-1244691f5a18",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 128
}