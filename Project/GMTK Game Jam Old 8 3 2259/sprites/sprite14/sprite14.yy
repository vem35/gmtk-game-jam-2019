{
    "id": "95358717-2863-4971-ab9c-6e0b4da06b33",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite14",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9d5da77-adc6-40df-84ca-6e1bb70846e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95358717-2863-4971-ab9c-6e0b4da06b33",
            "compositeImage": {
                "id": "991bb1ff-3a9d-4d8f-81c1-e31452c8d9d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9d5da77-adc6-40df-84ca-6e1bb70846e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97fbfd19-1e88-4671-95f6-cab9f9e9d101",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9d5da77-adc6-40df-84ca-6e1bb70846e1",
                    "LayerId": "5f27a62b-e7b8-483c-85ac-4e4858cad558"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5f27a62b-e7b8-483c-85ac-4e4858cad558",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95358717-2863-4971-ab9c-6e0b4da06b33",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}