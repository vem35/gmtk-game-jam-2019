{
    "id": "3538bd69-4c94-47d6-828f-ea8c4510014a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0253f0b8-7626-489c-b63f-8ee6b88d4633",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3538bd69-4c94-47d6-828f-ea8c4510014a",
            "compositeImage": {
                "id": "6f62b027-73c7-401a-b494-78f9551e37e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0253f0b8-7626-489c-b63f-8ee6b88d4633",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a992a65-7864-4bb3-8f58-29601e5bdd40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0253f0b8-7626-489c-b63f-8ee6b88d4633",
                    "LayerId": "de786131-99c3-4924-aa4e-f65bccbe4b98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "de786131-99c3-4924-aa4e-f65bccbe4b98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3538bd69-4c94-47d6-828f-ea8c4510014a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}