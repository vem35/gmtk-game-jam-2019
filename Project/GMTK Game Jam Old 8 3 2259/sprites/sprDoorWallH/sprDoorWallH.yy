{
    "id": "c28ebac6-dedd-4d94-b317-66064d3c0712",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprDoorWallH",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5bf1dcb8-d306-4d20-8ee6-dfb0c72be641",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c28ebac6-dedd-4d94-b317-66064d3c0712",
            "compositeImage": {
                "id": "b6bfcf5e-acea-4c2a-9341-2cf5b5f8b1f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bf1dcb8-d306-4d20-8ee6-dfb0c72be641",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b129464-12f3-441e-9992-b2f67e3c13da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bf1dcb8-d306-4d20-8ee6-dfb0c72be641",
                    "LayerId": "704b8897-5f13-4cac-8386-390af6254877"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "704b8897-5f13-4cac-8386-390af6254877",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c28ebac6-dedd-4d94-b317-66064d3c0712",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}