{
    "id": "64f6d324-ecb8-4792-a0de-afe7054cb734",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprDoorWallV",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "84fb3911-28d4-4423-9d85-65b7c66e2eae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64f6d324-ecb8-4792-a0de-afe7054cb734",
            "compositeImage": {
                "id": "640b38e5-1e6e-48a3-95ba-05875dd91ef2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84fb3911-28d4-4423-9d85-65b7c66e2eae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e5f9d98-bc19-48ad-a948-b30a4f917657",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84fb3911-28d4-4423-9d85-65b7c66e2eae",
                    "LayerId": "c7611419-8468-431a-8935-ae79885ded4c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "c7611419-8468-431a-8935-ae79885ded4c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64f6d324-ecb8-4792-a0de-afe7054cb734",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}