{
    "id": "2de89a82-211d-4505-ab3b-0e148034fa5f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBugIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 126,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3924f1cb-bb40-4d4d-b4da-6cad8439e5e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2de89a82-211d-4505-ab3b-0e148034fa5f",
            "compositeImage": {
                "id": "36d01a2c-8c1b-47ea-99d7-59f50e39842a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3924f1cb-bb40-4d4d-b4da-6cad8439e5e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6cc49f3-465f-41c5-b9b5-24c240e93b16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3924f1cb-bb40-4d4d-b4da-6cad8439e5e1",
                    "LayerId": "d6e54cea-acda-4b45-8781-7d7c4db370a5"
                }
            ]
        },
        {
            "id": "c527b163-21bd-4078-943a-4be8ca26963c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2de89a82-211d-4505-ab3b-0e148034fa5f",
            "compositeImage": {
                "id": "17aa5cd3-d876-469e-bf43-50c8fc252780",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c527b163-21bd-4078-943a-4be8ca26963c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c6ce190-3a54-4c52-8719-976cb22ab26d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c527b163-21bd-4078-943a-4be8ca26963c",
                    "LayerId": "d6e54cea-acda-4b45-8781-7d7c4db370a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d6e54cea-acda-4b45-8781-7d7c4db370a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2de89a82-211d-4505-ab3b-0e148034fa5f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 98,
    "xorig": 49,
    "yorig": 64
}