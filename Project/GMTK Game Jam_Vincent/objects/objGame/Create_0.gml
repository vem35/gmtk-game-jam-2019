/// @description Creates the global variables for the room
// You can write your code in this editor


if(instance_number(self) > 1)
{
	var loc = [xLocation, yLocation]
	with instance_find(self, 0)
	{
		xLocation = loc.x
		yLocation = loc.y
	}

	instance_destroy(self)
	return
}

map = ds_map_create()

