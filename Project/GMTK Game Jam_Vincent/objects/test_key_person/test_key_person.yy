{
    "id": "c95d72ad-cb84-4a6d-ac24-f5d699509e77",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "test_key_person",
    "eventList": [
        {
            "id": "51d69f8a-549e-4777-a68f-d72c0c9665c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c95d72ad-cb84-4a6d-ac24-f5d699509e77"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3cd3c0d5-9a59-4a00-8db4-22c16430cba5",
    "visible": true
}