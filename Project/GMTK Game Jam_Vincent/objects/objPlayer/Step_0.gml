/// @description TODO Player Movement

//controls are arrow keys to move, up arrow jump, Z pick up door, X shoot, space switch rooms

if(keyboard_check(vk_space)) global.time_scale = 0.1
else global.time_scale = 1

var vx_intend = hsp;
var vy_intend = vsp;

//show_debug_message(variable_global_get("time_scale"))

if(control > 0)
{
	
	var intend = scr_player_move_control(vx_intend, vy_intend)
	vx_intend = intend[0]
	vy_intend = intend[1]
	
}

if(not grounded)
{

	if(hasDoor)
		vy_intend -= grav * door_grav_mod * global.time_scale;
	else
		vy_intend -= grav * global.time_scale
	
	if(abs(vy_intend) > teminal_v)
		vy_intend = -teminal_v * door_grav_mod
		
}

var vel = scr_player_motion(vx_intend, vy_intend)
scr_player_platform()

hsp = vel[0]
vsp = vel[1]


//door stuff -------------------------

//door placement
if(scr_input(gcontrols.interact, cinput.press))
	show_debug_message("what")

if(not hasDoor)
{
	if(scr_input(gcontrols.interact, cinput.press))
	{
		show_debug_message("what")
		var p = instance_place(x, bbox_bottom + COLLISION_DETECTION, door)
		
		if(not p) p = instance_place(x, bbox_top - COLLISION_DETECTION, door)  
		if(not p) p = instance_place(bbox_left - COLLISION_DETECTION, y, door)
		if(not p) p = instance_place(bbox_right + COLLISION_DETECTION, y, door)

		show_debug_message(p)
		if(p)
		{
			instance_destroy(door)
			sprite_index = sprPlayerDoor
			hasDoor = true
		}
	}
}
else
{
	if(scr_input(gcontrols.interact, cinput.press))
	{

		var p = instance_place(x, bbox_bottom + COLLISION_DETECTION, doorSpotV)
		var vertical = true
		

		if(not p) p = instance_place(bbox_left - COLLISION_DETECTION, y, doorSpotH) 
		if(not p) p = instance_place(bbox_right + COLLISION_DETECTION, y, doorSpotH) else vertical = false
		if(not p) p = instance_place(x, bbox_top - COLLISION_DETECTION, doorSpotV)  else vertical = false
		
		if(p)
		{
			instance_create_layer(p.x, p.y, "Door", objDoor)
			hasDoor = false
			door.onWall = not vertical
			sprite_index = sprPlayer;
		}
	}
	
}