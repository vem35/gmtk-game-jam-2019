/// @description Insert description here
// You can write your code in this editor

switch(state)
{
	//DORMANT STATE
	case "IDLE":	
		
		if(distance_to_object(obj_following) < range)
		{
			ins_following = instance_nearest(x, y, obj_following)
			state = "FOLLOW";
		}
		break;
	//FOLLOWING STATE
	case "FOLLOW":
		if(ins_following)
		{
			var v = scr_move_to_point(ins_following.x, ins_following.y, hspd, vspd, acceleration, move_speed);
			hspd = v[0]
			vspd = v[1]
			
			if ((hspd != 0 and vspd != 0) and (keyboard_check_pressed(vk_space)))
			{
				state = "KNOCK"
			}
		}
		break;
	case "KNOCK":
		var result = scr_accelerate(hspd, vspd, 0, 0.5)
		
		//if(result[2])
			//game_end()
		
		break;
	default:
		
		show_debug_message("whoops " + string(current_time) + " " + string(state))
		state = "IDLE";
}
