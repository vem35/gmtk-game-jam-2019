{
    "id": "63772afc-cdf8-48e8-8ece-6c4188130c2c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSolid",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "3b6d388c-d9af-4eea-a869-df74bea26bee",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "directional.north",
                "directional.south",
                "directional.east",
                "directional.west"
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "directional.north",
            "varName": "col_dir",
            "varType": 6
        },
        {
            "id": "fd4df235-ae55-4c50-9483-b57bacfab187",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "10",
            "varName": "margin",
            "varType": 1
        }
    ],
    "solid": false,
    "spriteId": "3538bd69-4c94-47d6-828f-ea8c4510014a",
    "visible": true
}