/// @description Insert description here
// You can write your code in this editor
following = noone

if(obj)
{
	following = instance_position(i_x, i_y, obj)	
}

if(following)
{
	with(following)
	{
		show_debug_message(string(enabled) + " " + string(other.enabled))
		enabled = other.enabled
	}
}