/// @description Insert description here
// You can write your code in this editor


p = instance_place(x, y, objPlayer)
if(p)
{
	if(scr_input(gcontrols.interact, cinput.press) and not p.hasDoor)
	{
			flip = true
	}
}

if(flip)
{

	enabled = !enabled
	if(following)
	{
		with(following)
		{
			show_debug_message(string(enabled) + " " + string(other.enabled))
			enabled = other.enabled
		}
	}
	
	flip = false	
}

if(enabled) image_index = 0;
else image_index = 1;
