

if(keyboard_check_pressed(vk_enter))
	open = true

if(open)
{

	if (not was_open)
	{
		left = player.x < x
		up = player.y < y
		var i = instance_place(x, y, objDoorWallH)
		if(i)
		{
			if (left)
				bounds = [i.right_x, i.right_y, i.right_w, i.right_h]
			else
				bounds = [i.left_x, i.left_y, i.left_w, i.left_h]
		}
		else
		{
			i = instance_place(x, y, objDoorWallV)
			if(i)
			{
				if (not up)
					bounds = [i.up_x, i.up_y, i.up_w, i.up_h]
				else
					bounds = [i.down_x, i.down_y, i.down_w, i.down_h]
			}
		}

		instance_deactivate_region(x, y, 1, 1, true, true)
		was_open = true
		open_elapsed = open_timer
	}
	else
	{
		image_alpha = 0.5
		open_elapsed -= delta_time / power(10, 6)
		
		if(open_elapsed < 0)
		{
			open = false
			open_elapsed = 0
		}
	}
	
	if(onWall)
	{
		var d = player.x - x

		if (left and d > 0)	
		{
			objCamera.doorOpenX = true;
			scr_move_cam(objCamera, self)
			//TODO need to make the character change room more smoothly
			player.x = bbox_right + abs(player.x - player.bbox_left) + padding
			open = false
		}
		else if (not left and d < 0)
		{
			objCamera.doorOpenX = true;
			scr_move_cam(objCamera, self)
			//TODO need to make the character change room more smoothly
			player.x = bbox_left - abs(player.x - player.bbox_right) - padding
			open = false
		}
	}
	else
	{
		var d = player.y - y
		if (up and d > 0)
		{
			objCamera.doorOpenY = true;	
			player.y = bbox_bottom + abs(player.y - player.bbox_top) + padding
			scr_move_cam(objCamera, self)
			//player.y = y + (sign(y - player.y) * 128)
			open = false
		}
		else if (not up and d < 0)
		{
			objCamera.doorOpenY = true;	
			player.y = bbox_top - abs(player.y - player.bbox_bottom) - padding
			scr_move_cam(objCamera, self)
			open = false
		}
	}
}
else
{
	image_alpha = 1
	if (was_open)
	{
		instance_activate_region(x, y, 1, 1, true)
		was_open = false

	}
}