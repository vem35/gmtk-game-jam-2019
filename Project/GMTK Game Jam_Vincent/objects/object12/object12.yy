{
    "id": "bd14312a-0315-4715-a72d-9b3e5aa9c781",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object12",
    "eventList": [
        {
            "id": "fe112ef3-9926-48ef-90db-97efc9b99d18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bd14312a-0315-4715-a72d-9b3e5aa9c781"
        },
        {
            "id": "9ec2f1a9-5243-49fa-b352-383d4276d20d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "bd14312a-0315-4715-a72d-9b3e5aa9c781"
        },
        {
            "id": "5876c4cb-193a-4169-b240-28722ca4cb74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bd14312a-0315-4715-a72d-9b3e5aa9c781"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "0e501836-0e6b-4a3a-b6ae-4a11676fa8cd",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "objPlayer",
            "varName": "attached",
            "varType": 5
        },
        {
            "id": "a7f0c489-ef9e-4af7-925e-bdd7f81abad6",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                "directional.north",
                "directional.south",
                "directional.east",
                "directional.west"
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "directional.north",
            "varName": "facing",
            "varType": 6
        },
        {
            "id": "e41b6f21-209e-48cf-81fe-08e1ad0f7f50",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "door",
            "varType": 3
        }
    ],
    "solid": false,
    "spriteId": "01c5b9ce-47d1-414c-9ce7-0bff53b1b7dd",
    "visible": true
}