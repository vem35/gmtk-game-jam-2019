{
    "id": "6708bc16-efdb-4599-a1f9-9bc90c1b08fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPlayerDoor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 12,
    "bbox_right": 50,
    "bbox_top": 24,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ca7fde2-a077-4ffa-b878-8de8243c51f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6708bc16-efdb-4599-a1f9-9bc90c1b08fc",
            "compositeImage": {
                "id": "8e344360-2b40-46ba-a033-16cd3aed6815",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ca7fde2-a077-4ffa-b878-8de8243c51f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc3588cb-d4d5-4796-8412-19931c894d17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ca7fde2-a077-4ffa-b878-8de8243c51f4",
                    "LayerId": "76540657-dc9d-4355-a88b-063d3cc4f9fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "76540657-dc9d-4355-a88b-063d3cc4f9fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6708bc16-efdb-4599-a1f9-9bc90c1b08fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}