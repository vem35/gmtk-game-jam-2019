{
    "id": "e626d5f6-73b1-43f7-bdb8-c4403aa6237e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBugHit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 884,
    "bbox_left": 106,
    "bbox_right": 897,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0b7002e-9d76-458b-8ac3-f2cd4870a724",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e626d5f6-73b1-43f7-bdb8-c4403aa6237e",
            "compositeImage": {
                "id": "b7e42632-3692-49de-b07b-a1bbcf537b2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0b7002e-9d76-458b-8ac3-f2cd4870a724",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "597b180f-e7b7-4e7d-8bb9-2fc663aaf17d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0b7002e-9d76-458b-8ac3-f2cd4870a724",
                    "LayerId": "5f994a3c-e8a1-4e56-a4e3-ef40a900e250"
                }
            ]
        },
        {
            "id": "197b3e4e-d9ce-4061-a03b-f95962a0f163",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e626d5f6-73b1-43f7-bdb8-c4403aa6237e",
            "compositeImage": {
                "id": "ab1af79f-cba9-4126-b3f2-1e7cf1f8506e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "197b3e4e-d9ce-4061-a03b-f95962a0f163",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87926bbf-f929-4ce6-bafd-23c4da15f185",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "197b3e4e-d9ce-4061-a03b-f95962a0f163",
                    "LayerId": "5f994a3c-e8a1-4e56-a4e3-ef40a900e250"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 910,
    "layers": [
        {
            "id": "5f994a3c-e8a1-4e56-a4e3-ef40a900e250",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e626d5f6-73b1-43f7-bdb8-c4403aa6237e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 930,
    "xorig": 465,
    "yorig": 455
}