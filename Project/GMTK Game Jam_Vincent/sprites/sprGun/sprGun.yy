{
    "id": "01c5b9ce-47d1-414c-9ce7-0bff53b1b7dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 65,
    "bbox_left": 8,
    "bbox_right": 100,
    "bbox_top": 64,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d95d3663-f984-44db-a286-f680ee0a9cda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01c5b9ce-47d1-414c-9ce7-0bff53b1b7dd",
            "compositeImage": {
                "id": "95ae828b-9e2a-42ce-9b4a-2bc501b03701",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d95d3663-f984-44db-a286-f680ee0a9cda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28343307-8ce7-4c5d-a19a-1e8e7c53b36a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d95d3663-f984-44db-a286-f680ee0a9cda",
                    "LayerId": "2c575537-b871-4616-9879-feb0b419bde6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "2c575537-b871-4616-9879-feb0b419bde6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "01c5b9ce-47d1-414c-9ce7-0bff53b1b7dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 8,
    "yorig": 64
}