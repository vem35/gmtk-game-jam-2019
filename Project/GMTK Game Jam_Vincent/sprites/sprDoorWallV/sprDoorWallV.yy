{
    "id": "33d95193-9ad3-4ec1-bb57-18b2b8d8c1da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprDoorWallV",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f5f5e09-9669-4a30-998f-688fb0bfc70a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33d95193-9ad3-4ec1-bb57-18b2b8d8c1da",
            "compositeImage": {
                "id": "656f186f-b527-437f-a643-b4531ade67e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f5f5e09-9669-4a30-998f-688fb0bfc70a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2725898-d0b9-45ce-9266-ee82ced9d630",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f5f5e09-9669-4a30-998f-688fb0bfc70a",
                    "LayerId": "00389f53-ce25-452b-9923-f35bcbd4f499"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "00389f53-ce25-452b-9923-f35bcbd4f499",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33d95193-9ad3-4ec1-bb57-18b2b8d8c1da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}