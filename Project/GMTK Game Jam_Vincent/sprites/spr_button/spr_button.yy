{
    "id": "bd85d6bc-cef4-49c6-b943-30684e2a83ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "754be804-c7df-4b81-9b77-c757d700636b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd85d6bc-cef4-49c6-b943-30684e2a83ca",
            "compositeImage": {
                "id": "377e4060-ded4-4ff8-b929-c2487c54715e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "754be804-c7df-4b81-9b77-c757d700636b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "460a2b2f-e2c3-4011-9310-5458540438b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "754be804-c7df-4b81-9b77-c757d700636b",
                    "LayerId": "519af06d-e7c2-42d9-96f7-79c9ddfdff3d"
                },
                {
                    "id": "ac6eb58f-dd45-4386-95b8-68f2d255253b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "754be804-c7df-4b81-9b77-c757d700636b",
                    "LayerId": "f1baf02a-d82f-4021-a105-147fa2376cc5"
                }
            ]
        },
        {
            "id": "6c5373dc-cadb-4d1a-a120-46cf95f25258",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd85d6bc-cef4-49c6-b943-30684e2a83ca",
            "compositeImage": {
                "id": "9300b8c2-ca60-4617-af5f-1658f0b0573b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c5373dc-cadb-4d1a-a120-46cf95f25258",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "820936b5-b444-4746-85d3-a4df7315927d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c5373dc-cadb-4d1a-a120-46cf95f25258",
                    "LayerId": "f1baf02a-d82f-4021-a105-147fa2376cc5"
                },
                {
                    "id": "fce57838-9784-4176-9e33-de293c54f93d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c5373dc-cadb-4d1a-a120-46cf95f25258",
                    "LayerId": "519af06d-e7c2-42d9-96f7-79c9ddfdff3d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "f1baf02a-d82f-4021-a105-147fa2376cc5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bd85d6bc-cef4-49c6-b943-30684e2a83ca",
            "blendMode": 3,
            "isLocked": false,
            "name": "Color",
            "opacity": 100,
            "visible": true
        },
        {
            "__type": "GMImageFolderLayer_Model:#YoYoStudio.MVCFormat",
            "id": "c4c3b3d0-0c67-4781-9e8b-3c9368ed4c67",
            "modelName": "GMImageFolderLayer",
            "mvc": "1.0",
            "SpriteId": "bd85d6bc-cef4-49c6-b943-30684e2a83ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer Group 1",
            "opacity": 100,
            "visible": false,
            "layers": [
                
            ]
        },
        {
            "id": "519af06d-e7c2-42d9-96f7-79c9ddfdff3d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bd85d6bc-cef4-49c6-b943-30684e2a83ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}