{
    "id": "3cd3c0d5-9a59-4a00-8db4-22c16430cba5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b530b579-0100-4e93-b541-678f9933ebbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3cd3c0d5-9a59-4a00-8db4-22c16430cba5",
            "compositeImage": {
                "id": "37a81676-3647-426e-b442-c00f10de450b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b530b579-0100-4e93-b541-678f9933ebbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "040eb3dd-678c-4ac3-a936-b8feb5978d4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b530b579-0100-4e93-b541-678f9933ebbf",
                    "LayerId": "b262ed2f-db9c-4384-ab36-0a8cea9af3f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b262ed2f-db9c-4384-ab36-0a8cea9af3f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3cd3c0d5-9a59-4a00-8db4-22c16430cba5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}