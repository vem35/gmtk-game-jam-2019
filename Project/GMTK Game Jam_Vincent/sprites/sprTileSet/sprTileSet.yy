{
    "id": "9a5744c3-71a8-4aa0-9c6f-6d8cfdc775d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTileSet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 128,
    "bbox_right": 1535,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d16c265-dace-4fbd-a0dc-d684a54f9810",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a5744c3-71a8-4aa0-9c6f-6d8cfdc775d5",
            "compositeImage": {
                "id": "4b1532cd-234e-4d87-9683-b430a589b293",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d16c265-dace-4fbd-a0dc-d684a54f9810",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e58f8170-4efe-403a-972e-b084810d3c3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d16c265-dace-4fbd-a0dc-d684a54f9810",
                    "LayerId": "c1c6b90c-8d51-407a-bef4-e524734334f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 552,
    "layers": [
        {
            "id": "c1c6b90c-8d51-407a-bef4-e524734334f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a5744c3-71a8-4aa0-9c6f-6d8cfdc775d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1641,
    "xorig": 0,
    "yorig": 0
}