var hsp = argument0
var vsp = argument1
var spd = point_distance(0, 0, hsp, vsp)

var hdir = hsp / spd
var vdir = vsp / spd


var s = argument2
var a = argument3

var less = spd < s;
//a = a * delta_time / power(10, 6)



if(less)
{
	show_debug_message(string(a - spd) + " " + string(s))
	spd = max(a - spd, s)
}
else
{
	show_debug_message(string(a + spd) + " " + string(s))
	spd = min(a + spd, s)
}

hsp = hdir * spd;
vsp = vdir * spd;

x += hsp
y += vsp

return [hsp, vsp, spd == s]
