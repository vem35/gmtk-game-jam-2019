var left = argument0
var right = argument1
var jump = argument2
var amt = argument3
var accel = argument4
var grounded = argument5

var spd = 0;
var j = false;

if(left)
{
	amt -= accel;
	if(amt < -1)
		amt = -1
}
else if (right)
{
	amt += accel;
	if (amt > 1)
		amt = 1
}
else if (amt != 0) 
{
	var a  = amt;
	
	
	amt -= sign(a) * accel
	if (sign(a) != sign(amt)) amt = 0
}

if(grounded and jump)
{
	j = true
}


//returns array: intended horizontal speed, intended to jump
return [spd, amt, j]