var vx = argument0
var vy = argument1

var m  = scr_player_move_input(scr_input(gcontrols.left), scr_input(gcontrols.right), scr_input(gcontrols.jump, cinput.press), walk_amt, accel, grounded)


vx = vx * max(1 - control, 0) + (m[1] * walk_speed * control)
	

walk_amt = m[1]
	
if (walk_amt > 0)
	look_x = directional.east
else if (walk_amt < 0)
	look_x = directional.west
	
if(m[2])
{
	if(not hasDoor)
		vy = jump;
	else
		vy = jump * door_jump_mod;
}
	
if(hasDoor)
{
	vx *= door_speed_mod
}
	
if(scr_input(gcontrols.up))
	look_y = directional.north
else if (scr_input(gcontrols.down))
	look_y = directional.south
else
	look_y = directional.center
	
return [vx, vy]