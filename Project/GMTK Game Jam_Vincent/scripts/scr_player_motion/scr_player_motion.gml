var vx = argument0
var vy = argument1

var sx = vx * global.time_scale
var sy = vy * global.time_scale

var iterations = 0
if (place_meeting(x + sx, y, objSolid))
{
	while(not place_meeting(x + sign(sx), y, objSolid) and iterations++ < MAX_ITERATIONS)
	{
		x += sign(sx)		
	}	

	sx = 0
	vx = 0
	//show_debug_message(++iterations)
}


if (place_meeting(x, y - sy - 1, objSolid))
{
	iterations = 0
	while(not place_meeting(x, y - sign(sy), objSolid) and iterations++ < MAX_ITERATIONS)
	{
		y -= sign(sy)
	}
	vy = 0
	sy = 0
}



x += sx
y -= sy


return [vx, vy]