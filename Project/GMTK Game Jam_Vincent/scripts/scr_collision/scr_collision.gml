var ins = instance_place(x, y, argument0)

if(not ins)
{
	return	
}

var right = x > ins.x
var down = y - ins.y > 0

var r = 0
var l = 0
var d = 0
var u = 0

if(right)
{
	var r = ins.bbox_right - bbox_left
	if(r > 0 and r <= ins.margin)
	{
		x += r + 1	
	}
}
else
{
	var l = bbox_right - ins.bbox_left
	if(l >= 0 and l <= ins.margin)
	{
		x -= l + 1	
	}
}

if(down)
{
	var d = ins.bbox_bottom - bbox_top

	if(d >= 0 and d <= ins.margin)
	{
		y += d + 1	
	}
}
else
{
	var u = bbox_bottom - ins.bbox_top

	if(u >= 0 and u <= ins.margin)
	{
		y -= u + 1	
	}
}

var iterations = 0
while(instance_place(x, y, ins) and ++iterations < MAX_ITERATIONS)
{
	var h = r - l
	var v = d - u
	if (abs(h) < abs(v))
	{
		x += 2 * sign(h)
	}
	else
	{
		y += 2 * sign(v)	
	}
}