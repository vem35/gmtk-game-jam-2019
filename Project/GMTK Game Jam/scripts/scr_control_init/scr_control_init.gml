///SCR_CONTROL_INIT: initializes the controls when needed.

//creates the controls if it doesn't exist
if(not variable_global_exists("controls") or !ds_exists(global.controls, ds_type_map))
{	
	global.controls = ds_map_create()
}

//clear the map to make the new controls
ds_map_clear(global.controls)

//the controls for the game
enum gcontrols {
	left,
	right,
	up,
	down,
	shoot,
	interact,
	jump,
	gend,
	restart
}

//the input types.
enum cinput {
	press, //when the player first presses the button
	hold, //when the player is holding the button
	release //when the player is releasing the button
}

///the controls of the game.
// index 0 is the control
// index 1 is the keyboard input
// index 2 is the controller input
var c = 
[
	[gcontrols.left, vk_left, gp_padl],
	[gcontrols.right, vk_right, gp_padr],
	[gcontrols.up, vk_up, gp_padu],
	[gcontrols.down, vk_down, gp_padd],
	[gcontrols.shoot, "C", gp_face3],
	[gcontrols.interact, "Z", gp_face2],
	[gcontrols.jump, "X", gp_face1],
	[gcontrols.gend, vk_escape, gp_start],
	[gcontrols.restart, "P", gp_select]
]


//adds all controls from c to global.controls
for (var i = 0; i < array_length_1d(c); ++i)
{
	var a = c[i]
	if(ord(a[1]) >= ord("A") and ord(a[1]) <= ord("Z"))
	{
		//if the keyboard input is a letter
		ds_map_add(global.controls, a[0], [ord(a[1]), a[2]]);	
	}
	else
	{
		//if the keyboard input isn't a letter
		ds_map_add(global.controls, a[0], [a[1], a[2]]);	
	}
}
	