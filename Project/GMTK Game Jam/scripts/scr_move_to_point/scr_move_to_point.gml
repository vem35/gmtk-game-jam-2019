// @description
//another test
// another one


var x1 = argument[0]
var y1 = argument[1]
var hspd = argument[2]
var vspd = argument[3]


//get the current speed magnitude
var spd = point_distance(0, 0, hspd, vspd)
var hsd = hspd / spd 
var vsd = vspd / spd

var l = point_distance(x, y, x1, y1)

//calculates the velocity direction in x and y
var dx = 0
var dy = 0;
if(l)
{
	dx = (x1 - x) / l
	dy = (y1 - y) / l
}
//test


//adjusts the speed based on acceleration
if(argument_count > 5)
{
	var a = argument[4]
	var max_s = argument[5]
	
	
	var ax = dx * a;
	var ay = dy * a;
	
	var mx = dx * max_s;
	var my = dy * max_s;
	
	var samedirx = sign(dx) == sign(vspd)
	var samediry = sign(dy) == sign(hspd)
	
	hspd += ax
	vspd += ay
	
	if (samedirx and abs(hspd) > abs(ax))
		hspd = mx;
	if (samediry and abs(vspd) > abs(ay))
		vspd = my
	if(spd > max_s)
	{
		hspd = hsd * max_s
		vspd = vsd * max_s
	}
	
}
else
{
		
	//gets distance to intended point
	var d = point_distance(x, y, x1, y1)//distance_to_point(x1, y1)
	if(not d)
		d = 0;

	//calculates the velocity direction in x and y
	var vx = 0
	var vy = 0;

	if(d)
	{
		var vx = (x1 - x) / d
		var vy = (y1 - y) / d
	}

	//calculates the horizontal and vertical velocity
	hspd = vx * spd;
	vspd = vy * spd;

	show_debug_message(d)




}
x += hspd;
y += vspd;

//prepares to return the velocity
var vel = [hspd, vspd]
return vel;
