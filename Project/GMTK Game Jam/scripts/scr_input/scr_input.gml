///SCR_INPUT: initializes the controls when needed.

// ARG 0: the buttonb
// ARG 1 (optional): the input type. hold by default

//RETURNS: True if the input is in that state. False otherwise or if invalid

//creates the controls if it doesn't exist already (for some reason)
if(not ds_exists(global.controls, ds_type_map))
{
	scr_control_init()	
}

//the button
var b = argument[0]

//the asked for state
var s = cinput.hold

if(argument_count > 1)
	s = argument[1]

//if the control doesn't exist in the list of controls
if not ds_map_exists(global.controls, b)
	return false

//get the inputs from the controls. Keyboard is first. controller is second
var i = global.controls[? b]

switch (s)
{
	case cinput.press:
		return keyboard_check_pressed(i[0]) or gamepad_button_check_pressed(0, i[1])
		break
	case cinput.release:
		return keyboard_check_released(i[0]) or gamepad_button_check_released(0, i[1])
		break	
	default: //for the hold state
		return keyboard_check(i[0]) or gamepad_button_check(0, i[1])
		break
}