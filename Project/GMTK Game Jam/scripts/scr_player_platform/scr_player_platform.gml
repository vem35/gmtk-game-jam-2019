var instance = instance_place(x, y + 1, objPlatform)
if(instance != noone)
{	
	onPlatform = true;
	
	x += instance.hspd * instance.dir
	
	y += instance.vspd * instance.dir
}