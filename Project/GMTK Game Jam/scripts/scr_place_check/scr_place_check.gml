
var o_h = argument[0];
var o_v = argument[1];

var d = directional.north;

if(argument_count > 2)
	d = argument[2]

var p = noone;
switch(d)
{
	case directional.north:
		p = instance_place(x, bbox_top - COLLISION_DETECTION, o_v) 
		if(p) break
		return scr_place_check(o_h, o_v, directional.south)
	case directional.south:
		p = instance_place(x, bbox_bottom + COLLISION_DETECTION, o_v)	
		if(p) break
		return scr_place_check(o_h, o_v, directional.east)
	case directional.east:
		p = instance_place(bbox_right + COLLISION_DETECTION, y, o_h)
		if(p) break
		return scr_place_check(o_h, o_v, directional.west)
	case directional.west:
		p =instance_place(bbox_left - COLLISION_DETECTION, y, o_h)		
		break
}
return [p, d]