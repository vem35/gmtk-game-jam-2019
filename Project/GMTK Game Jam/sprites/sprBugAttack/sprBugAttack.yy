{
    "id": "cc5633dd-90a6-4bd1-9a5d-6220c36d7f65",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBugAttack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 269,
    "bbox_left": 3,
    "bbox_right": 191,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69f53d3d-50b0-47ce-9ff7-88f3f63f66d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc5633dd-90a6-4bd1-9a5d-6220c36d7f65",
            "compositeImage": {
                "id": "72346cfa-f3bc-4b3e-a1de-a00959d88562",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69f53d3d-50b0-47ce-9ff7-88f3f63f66d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e828852-1cec-4d77-b298-b617a55d63ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69f53d3d-50b0-47ce-9ff7-88f3f63f66d2",
                    "LayerId": "a2fcfc82-6d86-49d9-80b7-d312d5e3795d"
                }
            ]
        },
        {
            "id": "3fb5feaf-a2ba-4b08-8ea5-16d8b8f94be3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc5633dd-90a6-4bd1-9a5d-6220c36d7f65",
            "compositeImage": {
                "id": "95724b8d-bfc1-4866-bd78-0135de38229e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fb5feaf-a2ba-4b08-8ea5-16d8b8f94be3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f26645c-7198-40c4-ac0c-4f73e5ab750b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fb5feaf-a2ba-4b08-8ea5-16d8b8f94be3",
                    "LayerId": "a2fcfc82-6d86-49d9-80b7-d312d5e3795d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "a2fcfc82-6d86-49d9-80b7-d312d5e3795d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc5633dd-90a6-4bd1-9a5d-6220c36d7f65",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 201,
    "xorig": 100,
    "yorig": 135
}