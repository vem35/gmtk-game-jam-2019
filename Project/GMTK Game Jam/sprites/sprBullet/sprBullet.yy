{
    "id": "c0b7f9c4-0af4-4bce-97f1-f2297f48c40e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 8,
    "bbox_right": 55,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c26f778-1720-4f24-aae9-512a37da0944",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0b7f9c4-0af4-4bce-97f1-f2297f48c40e",
            "compositeImage": {
                "id": "fe7890b9-3bef-4d3a-a4a2-b741ebe7dcbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c26f778-1720-4f24-aae9-512a37da0944",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65a214b2-f1d4-42e1-b869-abfdacf6282e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c26f778-1720-4f24-aae9-512a37da0944",
                    "LayerId": "26b419c6-d1ad-4b4d-895e-bd167442be0e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "26b419c6-d1ad-4b4d-895e-bd167442be0e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0b7f9c4-0af4-4bce-97f1-f2297f48c40e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}