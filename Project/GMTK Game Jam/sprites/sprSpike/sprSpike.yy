{
    "id": "d861a662-4f59-450a-b1b7-da7b94a9c265",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSpike",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 65,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ce30c5c-3db3-4e05-a45a-6c77961c138e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d861a662-4f59-450a-b1b7-da7b94a9c265",
            "compositeImage": {
                "id": "c5cdb366-f00f-48eb-b714-54cf8cd453f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ce30c5c-3db3-4e05-a45a-6c77961c138e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f61590b5-704a-42ba-921e-b493d756b79a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ce30c5c-3db3-4e05-a45a-6c77961c138e",
                    "LayerId": "231ea308-7ea2-4e70-aef5-582aab5e56e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "231ea308-7ea2-4e70-aef5-582aab5e56e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d861a662-4f59-450a-b1b7-da7b94a9c265",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}