{
    "id": "77208f96-94a8-4d3b-b35f-9a2c139b1882",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "so",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a051fab5-0051-4430-b81f-5cfbb7fd1190",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77208f96-94a8-4d3b-b35f-9a2c139b1882",
            "compositeImage": {
                "id": "6d61fa80-bf7a-4483-ad7e-95ef882a9dc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a051fab5-0051-4430-b81f-5cfbb7fd1190",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "261e559c-62ef-44da-87d9-6bcf8b0cb838",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a051fab5-0051-4430-b81f-5cfbb7fd1190",
                    "LayerId": "8185a090-4d28-443d-a1ac-36e0da4343b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8185a090-4d28-443d-a1ac-36e0da4343b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77208f96-94a8-4d3b-b35f-9a2c139b1882",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}