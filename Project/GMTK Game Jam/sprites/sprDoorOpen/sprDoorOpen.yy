{
    "id": "37b2340c-9fa6-4a38-861f-e5d223ceb302",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprDoorOpen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dbf18145-bf82-4c90-9e93-fe1b3ec0691b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37b2340c-9fa6-4a38-861f-e5d223ceb302",
            "compositeImage": {
                "id": "4116b576-dcce-4ced-a5c7-796f0ddae7b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbf18145-bf82-4c90-9e93-fe1b3ec0691b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af39f89f-998b-4496-b544-6408f5e799c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbf18145-bf82-4c90-9e93-fe1b3ec0691b",
                    "LayerId": "02d15349-f6a3-4268-b3d5-4810b30273ad"
                }
            ]
        },
        {
            "id": "4c9ce7cd-8211-402f-b38e-6e29e8e4cfad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37b2340c-9fa6-4a38-861f-e5d223ceb302",
            "compositeImage": {
                "id": "e94f6c99-d605-4844-890c-7c995616d60a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c9ce7cd-8211-402f-b38e-6e29e8e4cfad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da8d93b3-ef92-49f8-85ea-1d50a4daf353",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c9ce7cd-8211-402f-b38e-6e29e8e4cfad",
                    "LayerId": "02d15349-f6a3-4268-b3d5-4810b30273ad"
                }
            ]
        },
        {
            "id": "098d7b43-186b-4d31-a6ac-2b617b2d3346",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37b2340c-9fa6-4a38-861f-e5d223ceb302",
            "compositeImage": {
                "id": "724edd4f-3d39-4eb1-9adb-a031b1b1b950",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "098d7b43-186b-4d31-a6ac-2b617b2d3346",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a6a3e8f-5c8c-4cfd-bccb-50ec4e349c56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "098d7b43-186b-4d31-a6ac-2b617b2d3346",
                    "LayerId": "02d15349-f6a3-4268-b3d5-4810b30273ad"
                }
            ]
        },
        {
            "id": "9451aac5-6fd0-42e8-b928-7697e7449d41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37b2340c-9fa6-4a38-861f-e5d223ceb302",
            "compositeImage": {
                "id": "43fe9d0b-9565-4561-aae0-562762177888",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9451aac5-6fd0-42e8-b928-7697e7449d41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36860c56-0da2-4c69-aa75-94752c5ecc98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9451aac5-6fd0-42e8-b928-7697e7449d41",
                    "LayerId": "02d15349-f6a3-4268-b3d5-4810b30273ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "02d15349-f6a3-4268-b3d5-4810b30273ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37b2340c-9fa6-4a38-861f-e5d223ceb302",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}