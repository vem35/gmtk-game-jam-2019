{
    "id": "3759b19b-e30b-4f75-8ad7-c9929ac67adb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite20",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 269,
    "bbox_left": 0,
    "bbox_right": 200,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f2f90de-5d10-464d-bd49-6828ceddf136",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3759b19b-e30b-4f75-8ad7-c9929ac67adb",
            "compositeImage": {
                "id": "07cd62aa-76a7-4efc-9903-dc4e9fb9c506",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f2f90de-5d10-464d-bd49-6828ceddf136",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "891154dd-3578-437c-8b5d-69cec58c81f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f2f90de-5d10-464d-bd49-6828ceddf136",
                    "LayerId": "28c54f4e-154b-4125-91af-e7cc50794cdd"
                }
            ]
        },
        {
            "id": "e4c72967-89ff-4993-8746-8aa2edf17156",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3759b19b-e30b-4f75-8ad7-c9929ac67adb",
            "compositeImage": {
                "id": "7e0a9f54-4aef-43dc-bead-4b5eda159728",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4c72967-89ff-4993-8746-8aa2edf17156",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e9f6805-1a19-4007-aa05-305d2054a52a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4c72967-89ff-4993-8746-8aa2edf17156",
                    "LayerId": "28c54f4e-154b-4125-91af-e7cc50794cdd"
                }
            ]
        },
        {
            "id": "c3d22d5d-bc02-4e2b-8682-3421f13a13d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3759b19b-e30b-4f75-8ad7-c9929ac67adb",
            "compositeImage": {
                "id": "27c0e939-5be6-4ed7-8073-6e8b790c2582",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3d22d5d-bc02-4e2b-8682-3421f13a13d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6836efaa-228f-4d49-9975-bef7fba99556",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3d22d5d-bc02-4e2b-8682-3421f13a13d6",
                    "LayerId": "28c54f4e-154b-4125-91af-e7cc50794cdd"
                }
            ]
        },
        {
            "id": "4450c5b9-dcc3-487c-b00a-adbb7a5f5702",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3759b19b-e30b-4f75-8ad7-c9929ac67adb",
            "compositeImage": {
                "id": "47a4a1e2-5df4-400e-9d79-62d35cb332ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4450c5b9-dcc3-487c-b00a-adbb7a5f5702",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb852239-cfcb-4c64-ab15-dee78309556e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4450c5b9-dcc3-487c-b00a-adbb7a5f5702",
                    "LayerId": "28c54f4e-154b-4125-91af-e7cc50794cdd"
                }
            ]
        },
        {
            "id": "9299a678-c9f0-45d5-bdf5-9c75b5728d0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3759b19b-e30b-4f75-8ad7-c9929ac67adb",
            "compositeImage": {
                "id": "cbe4ed02-e02e-4dd0-aa64-42a6ce6200cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9299a678-c9f0-45d5-bdf5-9c75b5728d0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b7e469d-5e4b-4ad7-a634-c23a4372dcac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9299a678-c9f0-45d5-bdf5-9c75b5728d0a",
                    "LayerId": "28c54f4e-154b-4125-91af-e7cc50794cdd"
                }
            ]
        },
        {
            "id": "15b5df33-3e33-4659-9880-6c3d0019d675",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3759b19b-e30b-4f75-8ad7-c9929ac67adb",
            "compositeImage": {
                "id": "93e689a7-4f51-42a4-993c-281a21409ca4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15b5df33-3e33-4659-9880-6c3d0019d675",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db5432f0-7afb-4d06-a936-0827bae5556f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15b5df33-3e33-4659-9880-6c3d0019d675",
                    "LayerId": "28c54f4e-154b-4125-91af-e7cc50794cdd"
                }
            ]
        },
        {
            "id": "eb0a53d6-e423-403c-8375-7ac1847c8c08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3759b19b-e30b-4f75-8ad7-c9929ac67adb",
            "compositeImage": {
                "id": "dfb389ea-6e77-45be-83d2-b41be4374a40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb0a53d6-e423-403c-8375-7ac1847c8c08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06754148-d507-465f-8b99-7463f59febd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb0a53d6-e423-403c-8375-7ac1847c8c08",
                    "LayerId": "28c54f4e-154b-4125-91af-e7cc50794cdd"
                }
            ]
        },
        {
            "id": "4190715a-eb9a-4244-9ab4-5d78be4cff8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3759b19b-e30b-4f75-8ad7-c9929ac67adb",
            "compositeImage": {
                "id": "8ae378d9-b5fc-4094-a658-966e348cdfd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4190715a-eb9a-4244-9ab4-5d78be4cff8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ece1aef7-48c8-4121-a849-18a9728c504a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4190715a-eb9a-4244-9ab4-5d78be4cff8e",
                    "LayerId": "28c54f4e-154b-4125-91af-e7cc50794cdd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "28c54f4e-154b-4125-91af-e7cc50794cdd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3759b19b-e30b-4f75-8ad7-c9929ac67adb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 201,
    "xorig": 36,
    "yorig": 122
}