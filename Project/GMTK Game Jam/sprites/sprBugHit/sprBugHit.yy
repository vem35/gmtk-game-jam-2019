{
    "id": "e626d5f6-73b1-43f7-bdb8-c4403aa6237e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBugHit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 241,
    "bbox_left": 10,
    "bbox_right": 189,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e94bf8f-98c7-4505-8b33-bafd42c2210b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e626d5f6-73b1-43f7-bdb8-c4403aa6237e",
            "compositeImage": {
                "id": "a7a2423c-2192-4308-af28-323635a173a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e94bf8f-98c7-4505-8b33-bafd42c2210b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a884bbd-0d42-4c50-9822-c33270e35e3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e94bf8f-98c7-4505-8b33-bafd42c2210b",
                    "LayerId": "5f994a3c-e8a1-4e56-a4e3-ef40a900e250"
                }
            ]
        },
        {
            "id": "aa7b3377-f80a-49ab-91e0-c138347d06e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e626d5f6-73b1-43f7-bdb8-c4403aa6237e",
            "compositeImage": {
                "id": "94edc9c8-1547-49b3-85d2-bf44263a2b41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa7b3377-f80a-49ab-91e0-c138347d06e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c022b04-f454-4624-ab62-ef8be72bd90f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa7b3377-f80a-49ab-91e0-c138347d06e1",
                    "LayerId": "5f994a3c-e8a1-4e56-a4e3-ef40a900e250"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "5f994a3c-e8a1-4e56-a4e3-ef40a900e250",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e626d5f6-73b1-43f7-bdb8-c4403aa6237e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 201,
    "xorig": 100,
    "yorig": 135
}