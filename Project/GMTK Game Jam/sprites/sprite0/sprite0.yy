{
    "id": "6ac175ef-7904-43d2-816d-c1cccd8ba06e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03aab313-6a50-4bab-99ef-325fdfe89122",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ac175ef-7904-43d2-816d-c1cccd8ba06e",
            "compositeImage": {
                "id": "0d8c8175-1f95-4815-b296-2038921f44cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03aab313-6a50-4bab-99ef-325fdfe89122",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b025fd43-a500-44bd-a46a-4c0721b54302",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03aab313-6a50-4bab-99ef-325fdfe89122",
                    "LayerId": "e7299654-662e-4d79-a437-6cf88b0b7f13"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e7299654-662e-4d79-a437-6cf88b0b7f13",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ac175ef-7904-43d2-816d-c1cccd8ba06e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}