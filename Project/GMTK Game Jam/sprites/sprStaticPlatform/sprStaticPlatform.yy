{
    "id": "94fff50f-150a-4581-b1fe-b8cc7caa2753",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprStaticPlatform",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f7e0a259-db12-476b-8f2e-98869b05b3e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94fff50f-150a-4581-b1fe-b8cc7caa2753",
            "compositeImage": {
                "id": "9f72b4aa-5480-4650-8818-0e8158d66dba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7e0a259-db12-476b-8f2e-98869b05b3e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faddeaac-f6c5-471f-9ce0-90b2d8f26bff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7e0a259-db12-476b-8f2e-98869b05b3e4",
                    "LayerId": "d270074a-1526-45ce-9f2c-378d3592ea0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d270074a-1526-45ce-9f2c-378d3592ea0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94fff50f-150a-4581-b1fe-b8cc7caa2753",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}