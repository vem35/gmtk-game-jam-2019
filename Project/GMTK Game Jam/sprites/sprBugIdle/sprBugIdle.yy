{
    "id": "2de89a82-211d-4505-ab3b-0e148034fa5f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBugIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 230,
    "bbox_left": 21,
    "bbox_right": 181,
    "bbox_top": 57,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c487615-88ba-48ed-af80-f2737354bc16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2de89a82-211d-4505-ab3b-0e148034fa5f",
            "compositeImage": {
                "id": "aafd3e47-cb93-44ec-b92a-041653df6e73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c487615-88ba-48ed-af80-f2737354bc16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be8021cf-2b65-4140-b088-be65e1506525",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c487615-88ba-48ed-af80-f2737354bc16",
                    "LayerId": "d6e54cea-acda-4b45-8781-7d7c4db370a5"
                }
            ]
        },
        {
            "id": "d248d06f-e531-45b9-8964-109cd8f45f27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2de89a82-211d-4505-ab3b-0e148034fa5f",
            "compositeImage": {
                "id": "0ab232f3-1327-4cf9-82b1-444d877f134a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d248d06f-e531-45b9-8964-109cd8f45f27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ba5c7ef-4eaf-4b3f-898f-9f27ae9be64c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d248d06f-e531-45b9-8964-109cd8f45f27",
                    "LayerId": "d6e54cea-acda-4b45-8781-7d7c4db370a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "d6e54cea-acda-4b45-8781-7d7c4db370a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2de89a82-211d-4505-ab3b-0e148034fa5f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 201,
    "xorig": 100,
    "yorig": 135
}