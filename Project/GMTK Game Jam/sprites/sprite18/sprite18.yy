{
    "id": "3974e317-4ae4-4924-877d-17d99161ce01",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite18",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fba05477-07bc-469e-9927-664950253eee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3974e317-4ae4-4924-877d-17d99161ce01",
            "compositeImage": {
                "id": "cae53144-cf6d-4f33-82e8-1397209a1ffa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fba05477-07bc-469e-9927-664950253eee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00a2e088-507b-4090-9abb-926b365ce44a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fba05477-07bc-469e-9927-664950253eee",
                    "LayerId": "e0961a37-6525-45b5-ab2d-bee06af6de16"
                },
                {
                    "id": "9a3c53bb-a7e4-4c5b-87a8-993586f42e06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fba05477-07bc-469e-9927-664950253eee",
                    "LayerId": "428b475d-a074-49d3-bc0e-784c345c2daf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e0961a37-6525-45b5-ab2d-bee06af6de16",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3974e317-4ae4-4924-877d-17d99161ce01",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "428b475d-a074-49d3-bc0e-784c345c2daf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3974e317-4ae4-4924-877d-17d99161ce01",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}