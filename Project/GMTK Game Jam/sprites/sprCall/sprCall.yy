{
    "id": "358ecd95-0188-4b2c-85c1-aef1e4ca6ae2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 0,
    "bbox_right": 109,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e28e6a4-299a-4a69-999e-14b4c0e31d43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "358ecd95-0188-4b2c-85c1-aef1e4ca6ae2",
            "compositeImage": {
                "id": "238888a9-31f8-4f4b-8d76-064dd75ac4ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e28e6a4-299a-4a69-999e-14b4c0e31d43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fa22498-f471-4e2b-91d7-1827ea835311",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e28e6a4-299a-4a69-999e-14b4c0e31d43",
                    "LayerId": "ad2c2964-7851-411d-990d-1939216faf53"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 250,
    "layers": [
        {
            "id": "ad2c2964-7851-411d-990d-1939216faf53",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "358ecd95-0188-4b2c-85c1-aef1e4ca6ae2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 110,
    "xorig": 0,
    "yorig": 0
}