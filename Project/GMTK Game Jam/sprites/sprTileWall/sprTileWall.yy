{
    "id": "268f0a0d-4689-4cf3-9c52-2867eb468cdd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTileWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 128,
    "bbox_right": 1535,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1cd49388-0aad-4bf1-ba4e-976e797d3444",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "268f0a0d-4689-4cf3-9c52-2867eb468cdd",
            "compositeImage": {
                "id": "d854fb05-2d2f-45b4-bb0f-23a35f7342a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cd49388-0aad-4bf1-ba4e-976e797d3444",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffedc9b2-815a-475c-a566-cde90cb74fed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cd49388-0aad-4bf1-ba4e-976e797d3444",
                    "LayerId": "eb63b446-855f-40fa-8b33-9e1220a1f82c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 552,
    "layers": [
        {
            "id": "eb63b446-855f-40fa-8b33-9e1220a1f82c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "268f0a0d-4689-4cf3-9c52-2867eb468cdd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1641,
    "xorig": 820,
    "yorig": 276
}