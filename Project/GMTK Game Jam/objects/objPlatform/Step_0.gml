/// @description ?

if(enabled)
{
	x += hspd * dir
	y += vspd * dir

	if(hspd && (x <= x_start || x >= x_end))
	{
		dir *= -1
	}
	else if (vspd && (y <= y_start || y >= y_end))
	{
		dir *= -1	
	}
}