/// @description Insert description here
// You can write your code in this editor
if (attached)
{
	x = attached.x
	y = attached.y
	
	if(attached.look_y == directional.center)
		facing = attached.look_x	
	else 
		facing = attached.look_y
		
	door = attached.hasDoor
}

if(not door)
{
	fireDelay = 0;

	fireDelay -= 1
	if(scr_input(gcontrols.shoot, cinput.press) && fireDelay < 0 && instance_number(objBullet) < 3)
	{
		var bx = bbox_right
		var by = bbox_top
	
		switch(facing)
		{
			case directional.north:
				by = bbox_top
				break
			case directional.south:
				by = bbox_bottom
				break
			case directional.east:
				bx = bbox_right
				break
			case directional.west:
				bx = bbox_left
				break
		}
	
	
		fireDelay = 3;
		var f = facing
		with(instance_create_layer(bx, by, "Player", objBullet))
		{
			hamt = 0
			vamt = 0
			switch(f)
			{
				case directional.north:
					vamt = -1
					break
				case directional.south:
					vamt = 1
					break
				case directional.west:
					hamt = -1
					break
				case directional.east:
					hamt = 1
					break
			}
		}
	}
}