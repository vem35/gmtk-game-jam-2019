{
    "id": "720e7b32-7ec5-408b-8dd1-16f47bdf3d1d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objPhone",
    "eventList": [
        {
            "id": "8e3bc41d-bb3e-4547-9fda-2aedc9650306",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 90,
            "eventtype": 9,
            "m_owner": "720e7b32-7ec5-408b-8dd1-16f47bdf3d1d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "8f17037b-3671-4dcb-9452-adb381179c6c",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"I am error\\nnew line\"",
            "varName": "text",
            "varType": 2
        }
    ],
    "solid": false,
    "spriteId": "358ecd95-0188-4b2c-85c1-aef1e4ca6ae2",
    "visible": true
}