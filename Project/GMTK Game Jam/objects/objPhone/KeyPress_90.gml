/// @description ?
if (point_in_circle(objPlayer.x, objPlayer.y, x, y, 200) && !instance_exists(oText))
{
	show_debug_message("message")
	with (instance_create_layer(x, y - 64, layer, oText))
	{
		text = other.text;
		length = string_length(text);
	}
}