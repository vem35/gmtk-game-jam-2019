/*/// @description TODO room transition code
camX = camera_get_view_x(cam);
camY = camera_get_view_y(cam);
row = (camX + areaW) / areaW;
column = (camY + areaH) / areaH;
cam_w_half = camX + (areaW / 2);
cam_h_half = camY + (areaH / 2);

//need to freeze the game while transition is happening

if(doorOpenX)
{
	if(!moveW){
		if(door.x > cam_w_half)
		{
			camDest = camX + areaW;
			moveW = true;
			dist = camDest;
		}
	
		if(door.x < cam_w_half)
		{
			camDest = camX - areaW;
			moveW = true;
			dist = camDest;
		}
	}


	if(moveW)
	{
		if(camSpeed < 0)
		{
			show_debug_message(camDest)
			camX = camDest;
			moveW = false
			doorOpenX = false;
		}
		else
		{
			var right = sign(camDest - camX) == 1;
			var dest = camX + sign(camDest - camX) * camSpeed;
			if (right && camDest < dest)
			{
				camX = camDest;
				moveW = false;
				doorOpenX = false;
			} 
			else if (!right && camDest > dest)
			{
				camX = camDest;
				moveW = false;
				doorOpenX = false;
			} 
			else 
			{
				camX = dest;
			}
		}
		camera_set_view_pos(cam, camX, camY);
		show_debug_message(string(camera_get_view_x(cam)))
	}
}

if(doorOpenY)
{
	if(!moveH){
		if(door.y > cam_h_half)
		{
			camDest = camY + areaH;
			moveH = true;
			dist = camDest;
		}
	
		if(door.y < cam_h_half)
		{
			camDest = camY - areaH;
			moveH = true;
			dist = camDest;
		}
	}


	if(moveH)
	{
		if(camSpeed < 0)
		{
			camY = camDest;
			moveH = false
			doorOpenY = false;
		}
		else
		{
			var down = sign(camDest - camY) == 1;
			var dest = camY + sign(camDest - camY) * camSpeed;
			if (down && camDest < dest)
			{
				camY = camDest;
				moveH = false;
				doorOpenY = false;
			} 
			else if (!down && camDest > dest)
			{
				camY = camDest;
				moveH = false;
				doorOpenY = false;
			} 
			else 
			{
				camY = dest;
			}
		}
		camera_set_view_pos(cam, camX, camY);
	}
}
*/


if(following)
{

	x = following.x
	y = following.y

	if(scr_view_location(self, directional.east) > max_x)
	{
		x = max_x - half_width
	}
	else if(scr_view_location(self, directional.west) < min_x)
	{
		x = min_x + half_width	
	}
	if(scr_view_location(self, directional.south) > max_y)
	{
		y = max_y - half_height
	}
	else if(scr_view_location(self, directional.north) < min_y)
	{
		y = min_y + half_height	
	}
	
}
