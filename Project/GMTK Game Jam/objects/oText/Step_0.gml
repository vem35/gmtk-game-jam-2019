
/// @description draws requested text

//makes letters show up one at a time
letters += spd;
textCurrent = string_copy(text, 1, floor(letters));


draw_set_font(fFont);
if(h == 0) h = string_height(text);
w = string_width(textCurrent);

//destroy when done
if(letters >= length && keyboard_check_pressed(ord("Z")))
{
	instance_destroy();
}