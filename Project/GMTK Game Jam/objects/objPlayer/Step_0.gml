/// @description TODO Player Movement

//controls are arrow keys to move, up arrow jump, Z pick up door, X shoot

if(keyboard_check(vk_space)) global.time_scale = 0.1
else global.time_scale = 1

var vx_intend = hsp;
var vy_intend = vsp;



if(control > 0)
{
	
	var intend = scr_player_move_control(vx_intend, vy_intend)
	vx_intend = intend[0]
	vy_intend = intend[1]
	
}

if(not grounded)
{

	if(hasDoor)
		vy_intend -= grav * door_grav_mod * global.time_scale;
	else
		vy_intend -= grav * global.time_scale
	
	if(abs(vy_intend) > teminal_v)
		vy_intend = -teminal_v * door_grav_mod
		
}

var vel = scr_player_motion(vx_intend, vy_intend)
scr_player_platform()

hsp = vel[0]
vsp = vel[1]


//door stuff -------------------------

//door placement
if(scr_input(gcontrols.interact, cinput.press))
	show_debug_message("what")

if(not hasDoor)
{
	if(scr_input(gcontrols.interact, cinput.press))
	{
		var i = scr_place_check(door, door);
		
		var p = i[0]


		if(p and not door.open)
		{
			instance_destroy(door)
			sprite_index = sprPlayerDoor
			hasDoor = true
		}
	}
}
else
{
	if(scr_input(gcontrols.interact, cinput.press))
	{
		
		var p = scr_place_check(doorSpotH, doorSpotV);
		var d = p[1]
		var o = p[0]
		if(o)
		{
			door = instance_create_layer(o.x, o.y, "Door", objDoor)
			hasDoor = false
			door.onWall = d == directional.west or d == directional.east
			show_debug_message(door.onWall)
			sprite_index = sprPlayer;
		}
	}
	
}


//damage----------------------
hitRecovery--
if(place_meeting(x, y, obj_flying_foe) && hitRecovery < 0)
{
	show_debug_message(life)
	hitRecovery = 60
	life--
}

if(place_meeting(x, y, objSpike))
{
	life = 0;	
}

if(life <= 0)
{
	//TODO game over screen?
	game_restart();	
}