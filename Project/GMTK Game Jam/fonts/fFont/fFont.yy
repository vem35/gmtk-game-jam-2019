{
    "id": "7efac1a6-f1e7-458b-bfb8-bbc4c8b2fabb",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fFont",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "72662902-d7f3-400a-b480-f94d7e6beaa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 61,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "57a1aaee-dc33-4258-9e24-cfa44114a6cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 61,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 222,
                "y": 191
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "f7c3adbf-07a9-4d4c-acf1-daee3427be7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 61,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 205,
                "y": 191
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "5e02b51e-bd70-4d52-82d7-bc6e48c6e5e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 61,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 174,
                "y": 191
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "1b2a8142-8ac3-4cce-87f9-d64dd901fe5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 26,
                "x": 146,
                "y": 191
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "2629a4bf-1491-440b-a3f0-d28d0a28b302",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 61,
                "offset": 3,
                "shift": 47,
                "w": 41,
                "x": 103,
                "y": 191
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b50e0a9e-74b7-4a83-93ec-2a96375e110c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 61,
                "offset": 2,
                "shift": 35,
                "w": 33,
                "x": 68,
                "y": 191
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "07c19378-957e-4d47-9e77-5073d125e3e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 61,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 60,
                "y": 191
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "d30a6993-b9ac-4d20-90f2-98477455c3ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 61,
                "offset": 3,
                "shift": 18,
                "w": 13,
                "x": 45,
                "y": 191
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "0d847acf-35b0-46a0-82dc-cfb63ff82272",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 61,
                "offset": 3,
                "shift": 18,
                "w": 13,
                "x": 30,
                "y": 191
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "47d3b7c5-ba5e-41b6-a54b-e2affcec81f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 61,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 231,
                "y": 191
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "f6bc3362-05e1-42f0-a1a3-98fbbb0eb254",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 61,
                "offset": 2,
                "shift": 31,
                "w": 26,
                "x": 2,
                "y": 191
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "ae6749ea-b303-4d81-80fa-d692895dbf3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 61,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 472,
                "y": 128
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "21682606-6d09-40a9-843b-fedaa382dadf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 61,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 455,
                "y": 128
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ddf8e941-e8ab-4d7e-be4b-726c6a03961d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 61,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 446,
                "y": 128
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "004a7f81-9001-45c4-9c75-7c8962b98ce6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 61,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 429,
                "y": 128
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e6c4cf2c-72c6-446b-aa94-2bf36943d8df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 402,
                "y": 128
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "2c7ad996-34f4-4ae0-805a-66e31f076eaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 61,
                "offset": 5,
                "shift": 29,
                "w": 15,
                "x": 385,
                "y": 128
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "9cc5ce29-b7ce-4584-b579-d35e216d3f77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 26,
                "x": 357,
                "y": 128
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "885aeb80-47e3-43d6-830a-045dfbe121d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 329,
                "y": 128
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "2a6e8430-ceca-4939-852d-4e1a7074438e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 61,
                "offset": 0,
                "shift": 29,
                "w": 27,
                "x": 300,
                "y": 128
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "eedae16a-c731-471c-aa6a-6de422ca0c18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 481,
                "y": 128
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1cd7fb24-149f-4f55-af30-2a6f632aa26d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 251,
                "y": 191
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "fcf2b930-7920-487d-b6de-8b05518108b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 279,
                "y": 191
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "bf239380-e3ff-439d-b9c1-51afd8c593a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 307,
                "y": 191
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "923e14f2-bc3e-4ba9-b16d-dc6e828e8536",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 424,
                "y": 254
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "21f69ca1-e6c6-47de-aaef-5836899eaaab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 61,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 415,
                "y": 254
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "67394efd-a5c6-46e0-ae4f-38ae4cbc89ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 61,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 406,
                "y": 254
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "bc34415a-d0e6-45b3-807e-e2194abb4b19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 61,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 377,
                "y": 254
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0a7134c7-0c5a-4d36-8b7f-d77bff998077",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 61,
                "offset": 2,
                "shift": 31,
                "w": 26,
                "x": 349,
                "y": 254
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "7ec2c437-f183-4ade-9fd2-e697234fcc9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 61,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 320,
                "y": 254
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "77a9964b-8017-4500-a229-3da39fdbd594",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 61,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 293,
                "y": 254
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d34b8aa6-12f6-4319-a16c-bc2b47ea63f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 61,
                "offset": 2,
                "shift": 54,
                "w": 50,
                "x": 241,
                "y": 254
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "1bc31a53-3fb8-4dae-b86a-b37e20059603",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 61,
                "offset": -1,
                "shift": 35,
                "w": 37,
                "x": 202,
                "y": 254
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "9c94133d-b785-4453-8baf-f493cbaef1ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 61,
                "offset": 3,
                "shift": 35,
                "w": 30,
                "x": 170,
                "y": 254
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "8543dae9-9b02-474f-b983-87921b13def8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 61,
                "offset": 2,
                "shift": 38,
                "w": 35,
                "x": 133,
                "y": 254
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "79cab049-86ee-462a-b315-97546cd751be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 61,
                "offset": 4,
                "shift": 38,
                "w": 32,
                "x": 99,
                "y": 254
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b50669ef-9297-401d-8fe1-61d5617a7815",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 61,
                "offset": 4,
                "shift": 35,
                "w": 29,
                "x": 68,
                "y": 254
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "055bbe08-cea9-43ee-b136-b2f59ba3f0ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 61,
                "offset": 4,
                "shift": 32,
                "w": 26,
                "x": 40,
                "y": 254
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "7a573e07-cf5f-4174-97af-4d7b8d54537f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 61,
                "offset": 2,
                "shift": 41,
                "w": 36,
                "x": 2,
                "y": 254
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d84c9d23-1a6b-425d-8e73-930e00947acb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 61,
                "offset": 4,
                "shift": 38,
                "w": 30,
                "x": 469,
                "y": 191
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "43600da6-3f99-4a78-b684-b92674174e97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 61,
                "offset": 4,
                "shift": 15,
                "w": 6,
                "x": 461,
                "y": 191
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "aa39efe4-258a-4e8a-804d-b112115a2f49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 61,
                "offset": 1,
                "shift": 27,
                "w": 22,
                "x": 437,
                "y": 191
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "2426105c-a4d7-429a-bb81-55c1022d3bcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 61,
                "offset": 3,
                "shift": 35,
                "w": 33,
                "x": 402,
                "y": 191
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "df3944d5-d49a-4c5b-8f06-dd6e313d1bf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 61,
                "offset": 3,
                "shift": 29,
                "w": 25,
                "x": 375,
                "y": 191
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "14490758-21c6-4527-b56d-ca8830f09e7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 61,
                "offset": 3,
                "shift": 44,
                "w": 38,
                "x": 335,
                "y": 191
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "9794e7a2-a83c-4ce6-8814-904275405c39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 61,
                "offset": 4,
                "shift": 38,
                "w": 30,
                "x": 268,
                "y": 128
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "7f5ee981-a215-4358-917d-8922ba05a44d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 61,
                "offset": 2,
                "shift": 41,
                "w": 37,
                "x": 229,
                "y": 128
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e5963622-3932-4bc5-a3ab-7f917f54896c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 61,
                "offset": 4,
                "shift": 35,
                "w": 30,
                "x": 197,
                "y": 128
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "3734aef7-0dea-4df0-9c2c-8df8154c02d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 61,
                "offset": 2,
                "shift": 41,
                "w": 38,
                "x": 117,
                "y": 65
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "2f9e57f7-6259-43b1-8486-98288ec5cc27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 61,
                "offset": 4,
                "shift": 38,
                "w": 34,
                "x": 68,
                "y": 65
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "43d926d4-ff7d-4d75-908b-aaa11107bb23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 61,
                "offset": 2,
                "shift": 35,
                "w": 31,
                "x": 35,
                "y": 65
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "28a87f3d-7458-43e2-96bf-2d3f2602a013",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 61,
                "offset": 1,
                "shift": 32,
                "w": 31,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "e18f3ac9-4b9a-4932-894d-d93b24831c52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 61,
                "offset": 4,
                "shift": 38,
                "w": 30,
                "x": 476,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0e66e633-8ea8-49a3-83a5-183762d90d2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 61,
                "offset": 0,
                "shift": 35,
                "w": 35,
                "x": 439,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "85547449-9572-43a2-b2d3-f84a182569ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 61,
                "offset": 0,
                "shift": 50,
                "w": 50,
                "x": 387,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "591d1701-457d-4626-b0a7-e543e09b52cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 61,
                "offset": 0,
                "shift": 35,
                "w": 36,
                "x": 349,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "9901769a-c7a6-4b84-b5c3-5a3e79cdffeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 61,
                "offset": 0,
                "shift": 35,
                "w": 35,
                "x": 312,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "90dbef37-f687-4df4-9a26-dcbe9e7ec060",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 61,
                "offset": 1,
                "shift": 32,
                "w": 31,
                "x": 279,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "5382ed25-be71-4e9d-9c8c-7d95c0211c08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 61,
                "offset": 3,
                "shift": 15,
                "w": 11,
                "x": 104,
                "y": 65
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "a798405a-8a14-4659-9423-3c7c1a7b8eb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 61,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 262,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "07c469b7-6996-4ee7-b01b-0a6835377960",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 61,
                "offset": 1,
                "shift": 15,
                "w": 11,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "09282f61-bf64-46be-83a1-c4441d566060",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 61,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 205,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "cdac810e-6c3e-400a-9345-e0cfe5cdf382",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 61,
                "offset": -1,
                "shift": 29,
                "w": 32,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "6d8be4d0-52f1-4f43-abfd-74efbd56f76c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 61,
                "offset": 2,
                "shift": 18,
                "w": 11,
                "x": 158,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "157ccb7b-b57b-44c8-9533-060c56dc65c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "575aa6e8-064b-4678-bfa1-39314277da7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 61,
                "offset": 3,
                "shift": 29,
                "w": 25,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "2b5efac8-0b11-45cd-9017-65e9d5147ac9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 61,
                "offset": 2,
                "shift": 27,
                "w": 25,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "1adf9a23-8b85-493c-a906-d136ba68e621",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 25,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "e0d9d204-2157-4921-8a0a-251dceaa0216",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a1be71c6-d5d5-4970-bb01-12376a3eee1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 61,
                "offset": 0,
                "shift": 15,
                "w": 17,
                "x": 243,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "10b10ec0-2a4d-4d0a-a7a6-9e3d0de3dfd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 25,
                "x": 157,
                "y": 65
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a0a4630c-8767-4f0d-ae56-8af28937448c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 61,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 413,
                "y": 65
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "d2d0307c-dc9d-45bb-a5ae-3b07eda8c4e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 61,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 184,
                "y": 65
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "dd30b4e9-65e0-476b-871d-d264e5fa3ad5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 61,
                "offset": -3,
                "shift": 12,
                "w": 12,
                "x": 157,
                "y": 128
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "caca760f-baf8-4559-8855-e5404ea9d54b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 61,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 131,
                "y": 128
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "52d41a93-46e5-45a8-8d67-9eb12b2b19cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 61,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 123,
                "y": 128
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "87922c45-a488-4655-9b68-fe69f3158d94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 61,
                "offset": 3,
                "shift": 44,
                "w": 38,
                "x": 83,
                "y": 128
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "29f190f9-71cc-4b82-b662-80b4b762070c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 61,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 58,
                "y": 128
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "54c38460-61d7-42a7-8353-0ac81b9354da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 29,
                "y": 128
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "39dcf5f6-db80-4795-ab39-dd9f17ec8905",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 61,
                "offset": 3,
                "shift": 29,
                "w": 25,
                "x": 2,
                "y": 128
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "8d841545-9e40-4670-844d-feaf8be62140",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 61,
                "offset": 1,
                "shift": 29,
                "w": 25,
                "x": 473,
                "y": 65
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "f4718a4d-b76c-486b-94d9-a7766d026b72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 61,
                "offset": 3,
                "shift": 18,
                "w": 16,
                "x": 455,
                "y": 65
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "7d09ee97-7adf-465f-9817-63b12741f0d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 61,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 171,
                "y": 128
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c4cb6026-f80b-4615-af1c-38421e9dda8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 61,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 438,
                "y": 65
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "6a796868-557a-4c87-8103-17b7810a7bea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 61,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 388,
                "y": 65
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "46508b36-9327-4c21-a6a5-2942de722e7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 61,
                "offset": 0,
                "shift": 27,
                "w": 26,
                "x": 360,
                "y": 65
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "6a06ac70-0405-4b2f-b657-1629dda54d23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 61,
                "offset": 0,
                "shift": 38,
                "w": 38,
                "x": 320,
                "y": 65
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "537d2d0a-9fac-4233-b5a4-8780aa2eb03d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 61,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 291,
                "y": 65
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "2d2c1572-0a7f-4a06-8af0-e6da37655be6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 61,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 262,
                "y": 65
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "cbef5d07-059a-4994-a768-ad2ba7264f6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 61,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 235,
                "y": 65
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "982f0032-f9c9-4af6-9dda-e3e6c86c8581",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 61,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 217,
                "y": 65
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "6dada0c4-08af-46a2-99ee-0ad437464f4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 61,
                "offset": 4,
                "shift": 14,
                "w": 5,
                "x": 210,
                "y": 65
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "8e2f7267-0935-4d59-b421-80b07ba9435e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 61,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 192,
                "y": 65
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b636ada2-abd8-4c04-b0c6-65a46faf4e21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 61,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 452,
                "y": 254
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "20001ffb-c8fd-45db-ba9f-29eb66884eab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 61,
                "offset": 10,
                "shift": 51,
                "w": 31,
                "x": 2,
                "y": 317
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "65a47fd3-28e3-41a0-b760-b322e13912b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 65
        },
        {
            "id": "c40cb2b8-81a5-488e-b24b-43fa74ec7774",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "e5214d15-dee8-4178-835b-2ba84a323218",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "d5c88880-9e41-4d1e-829d-81d67c1d70b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 902
        },
        {
            "id": "cfa5a10e-6707-4a24-a354-9806025d149e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 913
        },
        {
            "id": "9dcc4da0-979e-455a-a136-b9600d273d9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 916
        },
        {
            "id": "fabcf15f-b919-4939-8054-e21d35e2da6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 923
        },
        {
            "id": "15d44ab0-bd6d-497e-82f8-b85d90d2a006",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 932
        },
        {
            "id": "dbf074ba-3a76-482e-9ac2-e4187dc1c452",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "1a687514-d743-4a1e-ac00-817f90efc24e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "a7986728-1c86-4294-8261-8da08ca95548",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 49,
            "second": 49
        },
        {
            "id": "6560c963-2b07-4f98-9c68-a06e808e8350",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 32
        },
        {
            "id": "88eac1a5-299f-477a-bb6a-47f8eeb9ec3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 84
        },
        {
            "id": "1332b05e-4206-4180-8ea5-2908bb3f064c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 86
        },
        {
            "id": "da10b2a1-4732-4c64-913f-63d9a8262f29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "5e4fd807-4928-49d5-a8de-93c4e8c94c2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 89
        },
        {
            "id": "f0fc874c-a54f-45a8-9f6a-83c916369750",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "da7264f2-55b9-4d4f-a3d8-2eb89c85b3d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "083c5c9b-507d-4f0d-b466-f1c306fddf70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "5015c17e-8bad-450c-a209-c742910e53d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 160
        },
        {
            "id": "2159b01f-d508-4196-84de-3af2f9b728ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 8217
        },
        {
            "id": "35d19f7e-9854-47bd-8645-def7e63b3afd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 44
        },
        {
            "id": "e0191887-b6e3-4dfa-bf9c-625a778c7b5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 46
        },
        {
            "id": "79eba8e4-e180-41f9-92c2-a4863f72c0ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 65
        },
        {
            "id": "9a0b925a-c28c-4662-923e-289827985a22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 32
        },
        {
            "id": "5ad193b3-0baa-4986-89e3-7c4d7ed10cd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 84
        },
        {
            "id": "7297fdb2-c054-4dc1-9a96-af3173ac6c63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 86
        },
        {
            "id": "7a6a8de6-b569-48dc-a079-25fa489f115c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 87
        },
        {
            "id": "79a044cc-d68e-4ec4-955d-7e18308ad1e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 89
        },
        {
            "id": "c92a2722-c173-4d14-b0f5-add6370b510d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 121
        },
        {
            "id": "8730e5d1-6ec5-4c7a-8288-2a395ef0797b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 160
        },
        {
            "id": "5a2db3ff-2378-4228-a6c1-001d5ad39242",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8217
        },
        {
            "id": "c3fe75dc-787c-42f9-b368-151e7e3dccc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "bc76809a-fe66-443a-9c30-a6504571fddb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 44
        },
        {
            "id": "e5a5fee2-2336-470a-9f2e-f92382a273ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 46
        },
        {
            "id": "39b8f304-c95e-45cb-b238-a84c0e845b34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 65
        },
        {
            "id": "8a618f8f-ab75-46b5-828f-a54fe00ef5ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "14363168-4954-456b-a59a-3eb58608148d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "2a9d221a-f6f3-4abc-b78f-b1a7c0574958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "d752aab3-95eb-4251-8e24-69ca94ed4fb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "09b8f1c8-7c0a-4716-bffd-aee6fe398536",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "d1b01835-327c-4181-983d-e39eb824c761",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 32
        },
        {
            "id": "d7c779a9-3414-46d8-ad8c-2fffbcde9ba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 44
        },
        {
            "id": "f37a1244-6312-443e-8f23-f0ebef7d290a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 45
        },
        {
            "id": "90ec67f1-4aef-4fce-a9db-cc090425232d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 46
        },
        {
            "id": "db65d9dc-425b-41ea-9f49-fff68ef43424",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 58
        },
        {
            "id": "70f3b6c9-6640-4342-ae15-a6b474701f57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 59
        },
        {
            "id": "08396cb3-2198-44f0-87bb-c85aa4804ff5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 65
        },
        {
            "id": "d7c1dbfd-b9ee-482c-8322-24e4ebb09d2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "a2cccd36-fc27-4bb3-bc23-2efa73c34bfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 97
        },
        {
            "id": "10b18bae-3634-4c4a-81cc-0605ee31a5ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 99
        },
        {
            "id": "1c98ffeb-506c-4fb5-be9d-b624ef2879ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 101
        },
        {
            "id": "e69698fc-af04-46c8-bb6e-b8f0b7b5da61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 105
        },
        {
            "id": "dae88cd9-6823-4c98-bef0-7820073f8ecc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 111
        },
        {
            "id": "54c26b88-3f6c-4e7e-97da-9f1249495fb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "137e2cf0-236a-46b4-8e9c-1153d1243b28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 115
        },
        {
            "id": "affdf6d9-a384-434e-86c9-bf505a8f67ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "26d40f81-4eb6-4cf7-9102-815d384e5a59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 119
        },
        {
            "id": "65d1b2f9-cfd3-4008-9975-83fbda021e1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 121
        },
        {
            "id": "c68e3825-29ab-427e-b57f-ac53dd1c1eda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 160
        },
        {
            "id": "8e804172-789c-44d1-bfe1-4f072cb2c2ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 173
        },
        {
            "id": "17c11269-3a68-4cf0-972a-646ddca12894",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 84,
            "second": 894
        },
        {
            "id": "9c45c948-6eb5-4211-b941-9d0c60dc8604",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 44
        },
        {
            "id": "f06815b2-eb17-4758-8c99-34e42963bf76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 45
        },
        {
            "id": "81a642de-8618-4087-8a94-4b2ce82cfc59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 46
        },
        {
            "id": "8fa7af62-9cdc-4318-8ac4-dbf7d8722f2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 58
        },
        {
            "id": "308f72da-dc68-4224-abd0-4372753fdbda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 59
        },
        {
            "id": "dcd277f0-9088-4a05-b7f9-0ec8fa984799",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 65
        },
        {
            "id": "6450ed7f-3fa3-4c28-87db-eb5bdacb0123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 97
        },
        {
            "id": "1c7dd0ea-4b6f-484b-bc05-70dd45f3efb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 101
        },
        {
            "id": "5fdcfaed-d18d-4765-bbd1-29e8bfd198cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "a6be14ce-f472-44ac-895f-692885e0e55d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 111
        },
        {
            "id": "03515b03-b662-4905-a69e-ae082af259d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 114
        },
        {
            "id": "4c0ef3d7-641a-4b13-9fcc-dcf2f5bd8f9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 117
        },
        {
            "id": "f11400b9-7d43-4d64-8c28-36a9724891fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 121
        },
        {
            "id": "c23f1e51-6221-4fb3-9db1-0c78f2408028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 173
        },
        {
            "id": "4c46da61-d20e-405e-9a8d-592c604d8e29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 894
        },
        {
            "id": "a0b98d13-7e22-48bd-b56a-1094532fd94b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 44
        },
        {
            "id": "f990e818-27b9-4c30-a124-50f5a32b1f63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "ec738c57-24e2-4bbb-a105-7f807af57500",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 46
        },
        {
            "id": "91457f76-458b-413d-aaee-0429e7e3c2e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "41410336-465e-4b97-927a-9b1f97e7fa3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "156b2376-4c55-4234-aa99-ccc9e536150f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 65
        },
        {
            "id": "0102aba6-365c-4aa1-914d-578b84defefe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 97
        },
        {
            "id": "eb7a7d1c-4daf-4e87-90cf-a219fe0bf2e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "16638d7c-d86a-482a-8408-578c0c492d60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "7f8e57ad-b181-4cbb-bfae-c6aa4c5c4e69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "c48f2518-0a72-4b39-9b70-356a804cd268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "0e265b01-5d7a-4bc5-93cb-6f231cbbe0ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "8796c5bb-bbdf-41bb-a04d-a49fb3827b8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "ffb07e37-4f0f-4588-87e0-726a71a822c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "66dd8b78-ebdd-4b21-b43d-5cba90160032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 44
        },
        {
            "id": "620b0fa0-1c4a-4c21-bae4-077ef4ecc4a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 45
        },
        {
            "id": "42c8a6cd-7e84-492c-acec-914f4e229aba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 46
        },
        {
            "id": "16c66a3b-9501-4193-bd0c-cd87a3f08880",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 58
        },
        {
            "id": "23927c1d-eddf-41a1-b739-ac797c1a7bbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 59
        },
        {
            "id": "b00e4c4c-5283-45d9-81d7-b50ec81bfd1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 65
        },
        {
            "id": "0b70b5d7-b493-4da3-be09-e54fdd95978c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 97
        },
        {
            "id": "4d446999-b95b-486a-bd17-edd571b2b884",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 101
        },
        {
            "id": "449397f3-ec55-41e3-b952-703c06bfe37b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 105
        },
        {
            "id": "fb2031b9-c7d0-4f52-85cf-c11ccdf1ac78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 111
        },
        {
            "id": "ec93cfde-dcab-4bea-a9b5-a9083fd999c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 112
        },
        {
            "id": "92951334-95d4-4f08-8e87-87cb5d27c97c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 113
        },
        {
            "id": "3f257997-6102-4b33-9a5f-1795bda39380",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 117
        },
        {
            "id": "b88c0730-6417-40e0-a054-f362474d7b81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 118
        },
        {
            "id": "71527176-0225-44f7-98f6-bb95a56b811f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "caf30692-fbdf-4264-8c1f-44005fd163b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 173
        },
        {
            "id": "430aec8c-8cd2-41a5-a693-bc0dedd05e2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 894
        },
        {
            "id": "dcc9add1-eabd-4171-9ec0-f4f56ef5a040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "a0cb7a2c-26cd-4bca-97d1-34d896844cd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "260766ec-8e3d-4104-a560-08b33ad4fab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 44
        },
        {
            "id": "a1a17219-2399-48b7-a904-cfc215bdafb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 46
        },
        {
            "id": "6defa656-13be-44c1-a623-83a320b5d2a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8217
        },
        {
            "id": "b6ce66fa-a438-423e-93d2-a18ac1b7f09e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 44
        },
        {
            "id": "4c0141b9-df80-4f5d-902a-461d5a239085",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 46
        },
        {
            "id": "de4a0dfb-f126-43a0-b822-12cd8d8eba5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 44
        },
        {
            "id": "9aff281c-a647-4039-b77c-65f7f7dd6a90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 46
        },
        {
            "id": "72be00bd-25f4-4be7-90a6-f8255cbb9d44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 44
        },
        {
            "id": "401827da-e841-40a8-b96f-42d150380387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 40,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}